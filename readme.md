# Steam game playtime booster

### Installation
The latest version of premade Steam game playtime booster can be downloaded here:
`git clone https://github.com/w7rus/steam-hb-package.git` or `https://github.com/w7rus/steam-hb-package/archive/master.zip`

Then, you should install dependencies from npm:
Execute `npm install --save steam-user jsonfile winston readline` inside **data** dir or run (install_dep.cmd | install_dep.sh)

### Getting started
Execute `node bot.js` inside **data** dir or run (run_script.cmd | run_script.sh)

### Config setup

**accountdata.login** - steam user login

**accountdata.password** - steam user password

**gamedata.gamesarray** - array of game id(s)

**botdata.skiplogin** - if true, skip login method checks & use account data from config (only for fast deploy)

### Multi-config usage
To run multiple instances of script for different accounts, simply create new json file with same config structure and different setup. On script execution you'll be asked to select config by its filename.
