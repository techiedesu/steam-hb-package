/*
*
*	Dependencies
*
*/

var winston = require('winston'),
	steamuser = require('steam-user'),
	jsonfile = require('jsonfile'),
	readline = require('readline');

/*
*
*	Other required shit
*
*/

var rl = readline.createInterface({input: process.stdin, output: process.stdout}),
	lg = new (winston.Logger)({transports: [new (winston.transports.Console)({colorize: true, level: 'debug'}),new (winston.transports.File)({level: 'info', timestamp: true, filename: 'bot.log', json: false})]}),
	client = new steamuser();

var msg01 = 'Define configuration file you want to use (ex. config_01.json): ',
	msg02 = 'Use account data from configuration file? (y/n or skip): ',
	msg03 = 'None or missing account data in configuration file!',
	msg04 = 'Username: ',
	msg05 = 'Password: ',
	msg06 = 'Would you like to save login details to configuration file? (y/n or skip): ',
	msg07 = 'You have successfully logged in!',
	msg08 = 'None or missing gameid(s) data in configuration file!',
	msg09 = 'Would you like to save gameid(s) details to configuration file? (y/n or skip): ',
	msg10 = 'GameId(s) (ex. 730 or 730+210+...): ';

/*
*
*	Functions
*
*/

function saveConf() {
	jsonfile.writeFile(file, config);
}

function loginIn() {
	client.logOn({
		accountName: username,
		password: password,
	});
}

rl.question(msg01, function(option) {
	if (option) {
		file = option;
		jsonfile.readFile(file, function(err, obj) {
			if (err != null) {
				console.log(err);
			} else {
				config = obj;
				stage2();
			}
		});
	}
});

function stage2() {
	if (config.botdata.skiplogin == false) {
		rl.question(msg02, function(option) {
			if (option == 'y') {
				if ((config.accountdata.login != '') && (config.accountdata.password != '')) {
					client.logOn({
						accountName: config.accountdata.login,
						password: config.accountdata.password,
					});
				} else {
					lg.error(msg03);
					rl.question(msg04, function(option) {
						username = option;
						rl.question(msg05, function(option) {
							password = option;
							rl.question(msg06, function(option) {
								if (option == 'y') {
									config.accountdata.login = username;
									config.accountdata.password = password;
									saveConf();
									loginIn();
								} else {
									loginIn();
								}
							});
						});
					});
				}
			} else {
				rl.question(msg04, function(option) {
					username = option;
					rl.question(msg05, function(option) {
						password = option;
						rl.question(msg06, function(option) {
							if (option == 'y') {
								config.accountdata.login = username;
								config.accountdata.password = password;
								saveConf();
								loginIn();
							} else {
								loginIn();
							}
						});
					});
				});
			}
		});
	} else if (config.botdata.skiplogin == true) {
		if ((config.accountdata.login != '') && (config.accountdata.password != '')) {
			client.logOn({
				accountName: config.accountdata.login,
				password: config.accountdata.password,
			});
		} else {
			lg.error(msg03);
			rl.question(msg04, function(option) {
				username = option;
				rl.question(msg05, function(option) {
					password = option;
					rl.question(msg06, function(option) {
						if (option == 'y') {
							config.accountdata.login = username;
							config.accountdata.password = password;
							saveConf();
							loginIn();
						} else {
							loginIn();
						}
					});
				});
			});
		}
	} else {
		process.exit(9);
	}
}

client.on('loggedOn', function() {
	lg.info(msg07);
	client.setPersona(1);
	if ((config.gamedata.gamesarray != '')) {
		if ((config.gamedata.gamesarray).indexOf("+") == -1) {
			if (config.gamedata.gamesarray.match(/\d{1,6}/)) {
				var gameid = parseInt(config.gamedata.gamesarray);
				client.gamesPlayed([gameid]);
				lg.info('You are now boosting game playtime of ' + gameid + ' game!');
			}
		} else {
			multiplegames = (config.gamedata.gamesarray).split('+');
			var multiplegamesarray = [];
			for (var key in multiplegames) {
				if (multiplegames[key].match(/\d{1,6}/)) {
					var gameid = parseInt(multiplegames[key]);
					multiplegamesarray.push(gameid);
					lg.info('You are now boosting game playtime of ' + gameid + ' game!');
				}
			}
			if (key == (multiplegames.length - 1)) {
				client.gamesPlayed(multiplegamesarray);
			}
		}
	} else {
		lg.error(msg08);
		rl.question(msg10, function(option) {
			ids = option;
			if ((ids).indexOf("+") == -1) {
				if (ids.match(/\d{1,6}/)) {
					var gameid = parseInt(ids);
					client.gamesPlayed([gameid]);
					lg.info('You are now boosting game playtime of ' + gameid + ' game!');
					rl.question(msg09, function(option) {
						if (option == 'y') {
							config.gamedata.gamesarray = ids;
							saveConf();
						}
					});
				}
			} else {
				multiplegames = (ids).split('+');
				var multiplegamesarray = [];
				for (var key in multiplegames) {
					if (ids.match(/\d{1,6}/)) {
						var gameid = parseInt(multiplegames[key]);
						multiplegamesarray.push(gameid);
						lg.info('You are now boosting game playtime of ' + gameid + ' game!');
					}
				}
				if (key == (multiplegames.length - 1)) {
					client.gamesPlayed(multiplegamesarray);
				}
				rl.question(msg09, function(option) {
					if (option == 'y') {
						for (var key in multiplegames) {
							var gameid = parseInt(multiplegames[key]);
							multiplegamesarray.push(gameid);
							config.gamedata.gamesarray += multiplegames[key] + "+";
							saveConf();
						}
					}
				});
			}
		});
	}
});

client.on('error', function(e) {
	lg.error(e);
});