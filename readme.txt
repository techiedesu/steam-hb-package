Package info:
	1.1 install_dep.cmd | Installs dependencies for script
	1.2 run_script.cmd | Runs script
		
Packaged by w7rus. https://w7rus.net/

Dependencies:
	steam-user
	winston
	readline
	jsonfile
	
Instructions:
	1. Run install_dep.cmd, wait until npm package manager completes with downloads.
	2. Run run_script.cmd, fill up required details and you are done!
	3. You can also edit botdata.json and set skiplogin to 1 for skipping the choose of login method on script startup. Also if you would need to change gameid(s) edit gamesarray option.
	